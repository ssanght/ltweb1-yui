![Capture.PNG](https://bitbucket.org/repo/KEeonB/images/3476158808-Capture.PNG)
ĐỒ ÁN WEB  NHÓM YUI

1.	CƠ SỞ DỮ LIỆU 

a.	Họp nhóm để đưa ra mô hình ER thứ 3 ngày 22 tháng 11 năm 2016 (cơ bản hoàn thành)

b.	Chiếu mô hình ER thành bảng và tạo cơ sở dữ liệu (1461350)

c.	Tìm kiếm thông tin về từng sản phẩm bao gồm tên sản phẩm, nhà sản xuất, loại sản phẩm,… và hình ảnh cho từng sản phẩm. Tìm 30 sản phẩm trong đó 1461458 : 15 sản phẩm, 1461576: 15 sản phẩm và nhập vào dữ liệu

d.	Viết sẵn câu truy vấn cơ bản: tìm sản phẩm theo tên sản phẩm, mã sản phẩm, loại sản phẩm,  nhà sản xuất,…

2.	THIẾT KẾ GIAO DIỆN

a.	Trang chủ (bao gồm cả thanh đăng nhập) - 1461458

b.	Trang đăng ký - 1461350

c.	Trang kết quả tìm kiếm( khi bấm tìm kiếm thì sẽ chuyển đến trang này) - 1461576

d.	Trang chi tiết sản phẩm - 1461641

e.	Trang giỏ hàng - 1461458

f.	Trang thanh toán - 1461350

g.	Trang nhập thông tin khách lạ - 1461576

ADMIN(đăng nhập bằng tài khoản admin)

h.	Trang quản lý sản phẩm - 1461641

i.	Trang quản lý loại sản phẩm - 1461458

j.	Trang quản lý nhà sản xuất - 1461350

k.	Trang quản lý đơn hàng - 1461576

l.	Trang quản lý tài khoản - 1461641

m.	Trang vẽ biểu đồ tình hình kinh doanh – tạm thời chưa làm

3.	XỬ LÝ

a.	1461458

-	Trang chủ: xử lý load 5 sản phẩm bán chạy nhất, phải xử lý dựa trên số lượng bán, chuyển đến trang thông tin sản phẩm khi bấm vào sản phẩm

-	Trang giỏ hàng: Hiển thị tất cả mặt hàng mà người dùng đã thêm vào, cho phép chỉnh sửa số lượng, size,.. ở trang này và thêm nút đặt hàng để đến trang đặt hàng(trang thanh toán).

-	Trang quản lý loại sản phẩm: thực hiện được các chức năng thêm, xóa sửa loại sản phẩm,…

b.	1461350

-	Trang đăng ký: kiểm tra các trường hợp đăng ký tài khoản của người dùng, thỏa điều kiện sẽ tạo mới tài khoản.

-	Trang thanh toán: lấy sản phẩm từ giỏ hàng, tính tổng số tiền và thực  hiện kiểm tra đăng nhập để xử lý việc mua hàng của khách hàng là khách lạ hay khách có tài khoản

-	Trang quản lý nhà sản xuất: thực hiện thêm xóa sửa nhà sản xuất

c.	1461576

-	Trang kết quả tìm kiếm:Thực hiện tìm kiếm sản phẩm theo một số tiêu chí như tên sản phẩm, nhà sản xuất, giá bán,…sắp xếp sản phẩm hiển thị theo giá bán(nếu có đủ tg).

-	Trang nhập thông tin khách lạ: Khách hàng đặt hàng mà chưa đăng nhập sẽ chuyển đến trang này. Yêu cầu khách hàng nhập thông tin cần thiết như sđt, họ tên, địa chỉ,…sau khi nhập xong sẽ thông báo đặt hàng thành công.

-	Trang quản lý đơn hàng:  hiển thị đơn hàng theo từng khách hàng, admin có thể dựa vào mã khách hàng, ngày đặt hàng,… để tìm kiếm đơn hàng.

d.	1461641

-	Trang chi tiết sản phẩm: Cho phép người dùng xem một số chi tiết về thông tin sản phẩm, nếu người dùng có nhu cầu sẽ đưa vào giỏ hàng hoặc mua ngay tức là vừa đưa vào hior hàng và chuyển trang đến giỏ hàng luôn.

-	Trang quản lý sản phẩm: thêm xóa sửa thông tin, hình ảnh  sản phẩm,..

-	Trang quản lý tài khoản: sửa chữa một số thông tin của khách hàng yêu cầu, không xem được mật khẩu.


**# Hướng dẫn #**

**# QUAN TRỌNG #**
![15451145_714012272088274_1404012009_n.png](https://bitbucket.org/repo/KEeonB/images/969305672-15451145_714012272088274_1404012009_n.png)

Làm chung 1 branch Debug, có ai Commit thì thông báo liền, và pull về (như hình) có conflict thì sửa liền

### **1. Cách chèn header vào từng file php** ###


```
#!php

<?php include 'HomePage.php'; ?>
</body>
</html>
```
cần phải đóng tag <body> và <html> vì 2 tag đó chưa đóng, nếu có thể thì mai mốt t sẽ thêm vào phần footer sau

### **2. Tạo file p** ###

tạo trong thư mục php